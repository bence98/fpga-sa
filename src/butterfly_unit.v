//////////////////////////////
// Elemi 2-DFT (butterfly)
// számító egység. 3 CLK
// késleltetés.
//
// Forrás: https://web.mit.edu/6.111/www/f2017/handouts/FFTtutorial121102.pdf
//////////////////////////////
module butterfly_unit#(
	parameter VALID_LEN=1
)(
	input clk,
	input rst,
	
	input [15:0] ai_re,
	input [15:0] ai_im,
	input [15:0] bi_re,
	input [15:0] bi_im,
	input [15:0] twiddle_re,
	input [15:0] twiddle_im,
	
	output reg [15:0] ao_re,
	output reg [15:0] ao_im,
	output reg [15:0] bo_re,
	output reg [15:0] bo_im,
	
	// Valid jel késleltető
	input [VALID_LEN-1:0] valid_in,
	output reg [VALID_LEN-1:0] valid_out
);

wire [15:0] a_delay_re;
wire [15:0] a_delay_im;
wire [VALID_LEN-1:0] mul_valid;

// P=B*Twiddle
wire [31:0] p_re;
wire [31:0] p_im;
// T=P felső bitjei az előjelbit nélkül
// lásd [FFTtutorial121102] 11. oldal teteje
wire [15:0] t_re = {p_re[30:16],|p_re[15:0]};
wire [15:0] t_im = {p_im[30:16],|p_im[15:0]};
wire [VALID_LEN+31:0] mul_vin={valid_in, ai_re, ai_im};
wire [VALID_LEN+31:0] mul_vout;
assign {mul_valid, a_delay_re, a_delay_im}=mul_vout;

complex_mul#(
	.VALID_LEN(VALID_LEN+32)
) mul(
	.clk(clk),
	.rst(rst),
	
	.a_re(twiddle_re),
	.a_im(twiddle_im),
	.b_re(bi_re),
	.b_im(bi_im),
	.p_re(p_re),
	.p_im(p_im),
	
	.valid_in(mul_vin),
	.valid_out(mul_vout)
);

// Kimeneti összeadók
always @(posedge clk)
	if(rst) begin
		ao_re<='b0;
		ao_im<='b0;
		bo_re<='b0;
		bo_im<='b0;
		valid_out<='b0;
	end else begin
		ao_re<=a_delay_re+t_re;
		ao_im<=a_delay_im+t_im;
		bo_re<=a_delay_re-t_re;
		bo_im<=a_delay_im-t_im;
		valid_out<=mul_valid;
	end

endmodule
