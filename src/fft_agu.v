//////////////////////////////
// Fő FFT címgeneráló egység.
//
// Forrás: https://web.mit.edu/6.111/www/f2017/handouts/FFTtutorial121102.pdf
//////////////////////////////
module fft_agu#(
	parameter MAX_FFT_LEVEL=9
)(
	input clk,
	input rst,
	
	input en,
	output fft_level_done,
	output fft_done,
	
	// forrás RAM
	output [8:0] rd_addr_a,
	output [8:0] rd_addr_b,
	
	input [31:0] rd_data_a,
	input [31:0] rd_data_b,
	
	output [18:0] rd_valid_in,
	input  [18:0] rd_valid_out,
	
	// cél RAM
	output [8:0] wr_addr_a,
	output [8:0] wr_addr_b,
	output wr_en,
	
	output [31:0] wr_data_a,
	output [31:0] wr_data_b
);

reg [11:0] cntr;
always @(posedge clk)
	if(rst)
		cntr<='b0;
	else if(en)
		cntr<=cntr+'b1;

wire [3:0] fft_level=cntr[11:8];
wire [7:0] bf_agu_cntr=cntr[7:0];

assign fft_done=fft_level==MAX_FFT_LEVEL;
assign fft_level_done=&bf_agu_cntr;

wire [7:0] addr_tw;
wire en_delay;

butterfly_twiddle_agu agu(
	.clk(clk),
	
	.fft_level(fft_level),
	.bf_index(bf_agu_cntr),
	.addr_a(rd_addr_a),
	.addr_b(rd_addr_b),
	.addr_tw(addr_tw),
	.valid_in(en),
	.valid_out(en_delay)
);

assign rd_valid_in={en_delay, rd_addr_a, rd_addr_b};

// twiddle faktor ROM
wire [15:0] tw_re;
wire [15:0] tw_im;
twiddle_rom twrom(
	.clk(clk),

	.addr_tw(addr_tw),
	.twiddle_re(tw_re),
	.twiddle_im(tw_im)
);

wire [18:0] bf_valid;
assign {wr_en, wr_addr_a, wr_addr_b}=bf_valid;

// butterfly műveletvégző
butterfly_unit#(
	.VALID_LEN(2*9+1)
) bfu(
		.clk(clk),
		.rst(rst),
		.ai_re(rd_data_a[31:16]),
		.ai_im(rd_data_a[15:0]),
		.bi_re(rd_data_b[31:16]),
		.bi_im(rd_data_b[15:0]),
		.twiddle_re(tw_re),
		.twiddle_im(tw_im),
		.ao_re(wr_data_a[31:16]),
		.ao_im(wr_data_a[15:0]),
		.bo_re(wr_data_b[31:16]),
		.bo_im(wr_data_b[15:0]),
		.valid_in(rd_valid_out),
		.valid_out(bf_valid)
	);
// wr_en-re visszaírni az adatot

endmodule
