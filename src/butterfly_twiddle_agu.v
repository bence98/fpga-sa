//////////////////////////////
// Butterfly és Twiddle
// címgeneráló egység.
// 1 CLK késleltetés.
//
// Forrás: https://web.mit.edu/6.111/www/f2017/handouts/FFTtutorial121102.pdf
//////////////////////////////
module butterfly_twiddle_agu#(
	parameter VALID_LEN=1
)(
	input clk,
	input [3:0] fft_level,
	input [7:0] bf_index,
	output reg [8:0] addr_a,
	output reg [8:0] addr_b,
	output reg [7:0] addr_tw,
	
	// Valid jel késleltető
	input [VALID_LEN-1:0] valid_in,
	output reg [VALID_LEN-1:0] valid_out
);

/* addr_tw=bf_index felső fft_level bitje */
wire [15:0] mask='hff00>>fft_level;

// Barrel shifter
wire [8:0] addr_as [15:0];
wire [8:0] addr_bs [15:0];

assign addr_as[0]={bf_index, 1'b0};
assign addr_bs[0]={bf_index, 1'b1};

genvar i;
generate for(i=1;i<15;i=i+1) begin
	assign addr_as[i]={addr_as[i-1][7:0], addr_as[i-1][8]};
	assign addr_bs[i]={addr_bs[i-1][7:0], addr_bs[i-1][8]};
end
endgenerate

always @(posedge clk) begin
		addr_a<=addr_as[fft_level];
		addr_b<=addr_bs[fft_level];
		addr_tw<=bf_index & mask[7:0];
		valid_out<=valid_in;
	end

endmodule
