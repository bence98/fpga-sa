//////////////////////////////
// Twiddle ROM
// 1 CLK késleltetés.
//////////////////////////////
module twiddle_rom(
	input clk,
	input [7:0] addr_tw,
	output reg [15:0] twiddle_re,
	output reg [15:0] twiddle_im
);

/* 512 pontos szinusz első negyedperiódusa (128 pont) */
reg [15:0] sin_lut [127:0];
initial $readmemh("sin_lut.mem", sin_lut);

wire [7:0] addr_iv=-addr_tw;

always @(posedge clk)
	// második félperiódus
	if(addr_tw[7]) begin
		// cos(fi)=-sin(fi-pi/4)
		// sin(fi)= sin(fi-pi/2)
		twiddle_re<=-sin_lut[addr_tw[6:0]];
		if(addr_tw[6:0]=='b0)
			// sin(pi/2)=1
			twiddle_im<='h7fff;
		else
			twiddle_im<=sin_lut[addr_iv[6:0]];
	// első félperiódus
	end else begin
		// cos(fi)= sin(fi+pi/4)
		// sin(fi)= sin(fi)
		if(addr_tw[6:0]=='b0)
			// cos(0)=1
			twiddle_re<='h7fff;
		else
			twiddle_re<=sin_lut[addr_iv[6:0]];
		twiddle_im<=sin_lut[addr_tw[6:0]];
	end

endmodule
