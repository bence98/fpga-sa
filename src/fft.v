module fft(
	input clk,
	input rst,
	input start,
	output done,
	
	// külső memóriainterfész
	input [8:0] ex_rd_addr,
	output [31:0] ex_rd_data,
	output ex_rd_valid,
	input [8:0] ex_wr_addr,
	input [31:0] ex_wr_data,
	output ex_wr_valid,
	input ex_wren
);
localparam NR_RAMS=4;

localparam RST=0;
// FFT szint feldolgozása
localparam LVL=1;
// várakozás a pipeline kiürülésére
localparam FLUSH=2;
// buffer swap
localparam SWAP=3;
// FFT kész
localparam DONE=4;

reg [2:0] state;
reg [2:0] next_state;

always @(posedge clk)
	if(rst)
		state<=DONE;
	else
		state<=next_state;

// állapotgép vezérlőjelek
wire lvl_done, wr_en, fft_done;

// állapotátmenet tábla
always @(*)
	case (state)
		RST: next_state<=LVL;
		LVL: if(lvl_done)  next_state<=FLUSH;
		     else          next_state<=LVL;
		FLUSH: if(wr_en)   next_state<=FLUSH;
		       else        next_state<=SWAP;
		SWAP: if(fft_done) next_state<=DONE;
		      else         next_state<=LVL;
		DONE: if(start)    next_state<=RST;
		      else         next_state<=DONE;
		default: next_state<=RST;
	endcase

// kimeneti logika
assign done=state==DONE;
wire next_done=next_state==DONE;

// buffer csere vezérlőjel
wire swap=state==SWAP;
// belső reset jel
wire rst_in=state==RST;

// DPBRAM-ok
reg  [8:0]  addr_as [NR_RAMS-1:0];
reg  [8:0]  addr_bs [NR_RAMS-1:0];
wire [31:0] data_as [NR_RAMS-1:0];
wire [31:0] data_bs [NR_RAMS-1:0];
reg  [31:0] din_as  [NR_RAMS-1:0];
reg  [31:0] din_bs  [NR_RAMS-1:0];
reg         wr_en_as[NR_RAMS-1:0];
reg         wr_en_bs[NR_RAMS-1:0];
reg  [18:0] vld_ins [NR_RAMS-1:0];
wire [18:0] vld_exs [NR_RAMS-1:0];

genvar i;
generate for(i=0;i<NR_RAMS;i=i+1) begin
dpbram#(
	.ADDR_WIDTH(9),
	.VALID_LEN(2*9+1)
) ram(
	.clk(clk),
	
	.addr_a(addr_as[i]),
	.addr_b(addr_bs[i]),
	.dout_a(data_as[i]),
	.dout_b(data_bs[i]),
	.din_a(din_as[i]),
	.din_b(din_bs[i]),
	.wr_en_a(wr_en_as[i]),
	.wr_en_b(wr_en_bs[i]),
	
	.valid_in(vld_ins[i]),
	.valid_out(vld_exs[i])
);
end
endgenerate

// számítási egység
wire en=state==LVL;

wire [8:0]  addr_a;
wire [8:0]  addr_b;
wire [31:0] data_a;
wire [31:0] data_b;
wire [18:0] vld_in;
wire [18:0] vld_out;
wire [8:0]  wadd_a;
wire [8:0]  wadd_b;
wire [31:0] din_a;
wire [31:0] din_b;

fft_agu agu(
	.clk(clk),
	.rst(rst_in),
	
	.en(en),
	.fft_done(fft_done),
	.fft_level_done(lvl_done),
	
	.rd_addr_a(addr_a),
	.rd_addr_b(addr_b),
	.rd_data_a(data_a),
	.rd_data_b(data_b),
	.rd_valid_in(vld_in),
	.rd_valid_out(vld_out),
	.wr_addr_a(wadd_a),
	.wr_addr_b(wadd_b),
	.wr_data_a(din_a),
	.wr_data_b(din_b),
	.wr_en(wr_en)
);

// swap multiplexer állapotgép
reg [3:0] mux_state;
reg [1:0] mux_sel_bank_rd;
reg [1:0] mux_sel_bank_wr;

always @(posedge clk)
	if(rst_in)
		mux_state<='b0;
	else if(swap)
		mux_state<=mux_state+'b1;

// TODO
always @(*)
	case(mux_state)
		0: begin
			mux_sel_bank_rd<='d0;
			mux_sel_bank_wr<='d1;
		end
		8: begin
			mux_sel_bank_rd<='d2;
			mux_sel_bank_wr<='d3;
		end
		default: if(mux_state[0]) begin
			mux_sel_bank_rd<='d1;
			mux_sel_bank_wr<='d2;
		end else begin
			mux_sel_bank_rd<='d2;
			mux_sel_bank_wr<='d1;
		end
	endcase

// bemenő adat cím bitsorrend csere
wire [8:0] ex_wr_addr_flip;
genvar k;
generate for(k=0;k<9;k=k+1) begin
	assign ex_wr_addr_flip[k]=ex_wr_addr[8-k];
end
endgenerate

// swap multiplexer
integer j;
always @(*)
	for(j=0;j<NR_RAMS;j=j+1) begin
		if(j==mux_sel_bank_rd) begin
			addr_as[j]<=addr_a;
			addr_bs[j]<=addr_b;
			vld_ins[j]<=vld_in;
			din_as[j]<='b0;
			din_bs[j]<='b0;
			wr_en_as[j]<='b0;
			wr_en_bs[j]<='b0;
		end else if(j==mux_sel_bank_wr) begin
			addr_as[j]<=wadd_a;
			addr_bs[j]<=wadd_b;
			vld_ins[j]<='b0;
			din_as[j]<=din_a;
			din_bs[j]<=din_b;
			wr_en_as[j]<=wr_en;
			wr_en_bs[j]<=wr_en;
		end else begin
			if(j=='b0) begin
				addr_as[j]<=ex_wr_addr_flip;
				din_as[j]<=ex_wr_data;
				wr_en_as[j]<=ex_wren;
			end else if(j==(NR_RAMS-1)) begin
				addr_as[j]<=ex_rd_addr;
				din_as[j]<='b0;
				wr_en_as[j]<='b0;
			end else begin
				addr_as[j]<='b0;
				din_as[j]<='b0;
				wr_en_as[j]<='b0;
			end
			addr_bs[j]<='b0;
			vld_ins[j]<='b0;
			din_bs[j]<='b0;
			wr_en_bs[j]<='b0;
		end
	end

assign data_a=data_as[mux_sel_bank_rd];
assign data_b=data_bs[mux_sel_bank_rd];
assign vld_out=vld_exs[mux_sel_bank_rd];

assign ex_rd_data=data_as[NR_RAMS-1];
assign ex_rd_valid=mux_sel_bank_wr!=(NR_RAMS-1);
assign ex_wr_valid=mux_sel_bank_rd!='b0;

endmodule
