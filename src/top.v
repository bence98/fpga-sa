`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Budapesti Mûszaki és Gazdaságtudományi Egyetem
// Engineer: Csókás Bence, Kiss Benedek
// 
// Create Date: 03/20/2022 12:26:44 PM
// Design Name: 
// Module Name: top
// Project Name: FPGA Spektrumanalizátor
// Target Devices: LOGSYS Kintex-7 board
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input clk100M,
    input rst,
    input fft_start,
    input fft_wr_en,
    
    // Bloxes VGA bõvítõmodul - A csatlakozó
    output [1:0] vga_r,
    output [1:0] vga_g,
    output [1:0] vga_b,
    output vga_hsync,
    output vga_vsync,
    
    output reg [7:0] led_r,
    output reg [7:0] led_g,
    output reg [7:0] led_b,
    
    // CS4270 I2S kodek
    output codec_rstb,
    output codec_m0,
    output codec_m1,
    output codec_mdiv1,
    output codec_mdiv2,
    output codec_i2s,
    
    output i2s_mclk,
    output i2s_bclk,
    output i2s_lrclk,
    output i2s_dout,
    input i2s_data
    );

wire [10:0] display_col;
wire [9:0] display_row;
wire [5:0] display_rgb;

clk_gen clk_mul(
	.clk_i(clk100M),
	.rst_i('b0),
	.clk_200(clk)
);

vga display(
    .clk(clk100M),
    .rst(rst),
    
    .red_out(vga_r),
    .green_out(vga_g),
    .blue_out(vga_b),
    .hsync_out(vga_hsync),
    .vsync_out(vga_vsync),
    
    .column_addr(display_col),
    .row_addr(display_row),
    
    .red_in(display_rgb[5:4]),
    .green_in(display_rgb[3:2]),
    .blue_in(display_rgb[1:0])
);

// FFT oszlopok: 16 pixel/oszlop
wire [8:0] bar_nr=display_col/2;

wire [1:0] sample_valid;
wire [23:0] sample_data;
reg  [23:0] aud_din0;
reg  [23:0] aud_din1;
codec_if codec(
	.clk(clk100M),
	.rst(rst),
	
	.codec_m0(codec_m0),
	.codec_m1(codec_m1),
	.codec_i2s(codec_i2s),
	.codec_mdiv1(codec_mdiv1),
	.codec_mdiv2(codec_mdiv2),
	.codec_rstn(codec_rstb),
	
	.codec_mclk(i2s_mclk),
	.codec_lrclk(i2s_lrclk),
	.codec_sclk(i2s_bclk),
	.codec_sdin(i2s_dout),  // I2S kimenet, nem használt
	.codec_sdout(i2s_data),
	
	.aud_dout_vld(sample_valid),
	.aud_dout(sample_data),
	
	// I2S kimenet, nem használt
	.aud_din_vld(2'b11),
	.aud_din_ack(),
	.aud_din0(aud_din0),
	.aud_din1(aud_din1)
);
// loopback
always @(posedge clk100M)
	if(sample_valid[0])
		aud_din0<=sample_data;

always @(posedge clk100M)
	if(sample_valid[1])
		aud_din1<=sample_data;

// FFT
wire [31:0] fft_data;
reg [9:0] fft_wr_addr;
wire fft_input_ready=fft_wr_addr[9];
wire fft_rd_valid;

reg [1:0] samples;

always @(posedge clk) begin
    samples <= {samples[0], sample_valid[0]};
end
wire sampleValidRise = samples == 2'b01;

always @(posedge clk)
	if(rst|fft_input_ready)
		fft_wr_addr<='b0;
	else if(sampleValidRise&fft_wr_en)
		fft_wr_addr<=fft_wr_addr+'b1;



fft fft_left(
	.clk(clk),
	.rst(rst),
	.start(fft_input_ready&fft_start),
	.done(),
	.ex_rd_addr(bar_nr), // TODO
	.ex_rd_data(fft_data),
	.ex_rd_valid(fft_rd_valid),
	.ex_wr_addr(fft_wr_addr[8:0]),
	.ex_wr_data({{5{sample_data[23]}}, sample_data[23:13], 16'b0}),
	.ex_wr_valid(),
	.ex_wren(sampleValidRise&fft_wr_en)
);

// bar chart kirajzolás

wire [15:0] fft_data_re=fft_data[31:16];
wire [15:0] fft_data_im=fft_data[15:0];
wire [15:0] fft_data_im_inv=-fft_data_im;
wire [31:0] fft_data_abs;

complex_mul abs_mul(
	.clk(clk),
	.rst(rst),
	.a_re(fft_data_re),
	.a_im(fft_data_im),
	.b_re(fft_data_re),
	.b_im(fft_data_im_inv),
	.p_re(fft_data_abs)
);

wire [9:0] fft_data_abs_abs = {10{fft_data_abs[31]}}^fft_data_abs[31:22];
assign display_rgb=fft_rd_valid&&display_row<=fft_data_abs_abs?'d12:'d0;

wire [23:0] sample_data_abs={24{sample_data[23]}}^sample_data;
always @(posedge clk) begin
    if (bar_nr == 0) begin
        led_r <= {8'hff} << (7-sample_data_abs[23:21]);
        led_g <= 8'h0;//{8'hff} << (7-fft_data_abs[19:17]);
        led_b <= {8'hff} << (7-fft_data_abs[22:20]);
    end
end

endmodule
