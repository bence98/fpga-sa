//////////////////////////////
// Komplex szorzó
//
// 2 CLK alatt összeszoroz két
// előjeles komplex számot az
// FPGA DSP48E1 slice-ait
// felhasználva (4 db-ot).
//
// Szerző: Csókás Bence
//////////////////////////////

module complex_mul#(
	parameter VALID_LEN=1
)(
	input wire clk,
	input wire rst,
	
	// A és B operandus (16 bit)
	input wire signed [15:0] a_re,
	input wire signed [15:0] a_im,
	input wire signed [15:0] b_re,
	input wire signed [15:0] b_im,
	
	// Szorzat (32 bit)
	output reg signed [31:0] p_re,
	output reg signed [31:0] p_im,
	
	// Valid jel késleltető
	input [VALID_LEN-1:0] valid_in,
	output reg [VALID_LEN-1:0] valid_out
);

reg signed [31:0] re_re;
reg signed [31:0] im_im;
reg signed [31:0] re_im;
reg signed [31:0] im_re;
reg signed [VALID_LEN-1:0] mul_valid;

reg signed [15:0] a_re_buf;
reg signed [15:0] a_im_buf;
reg signed [15:0] b_re_buf;
reg signed [15:0] b_im_buf;
reg [VALID_LEN-1:0] mul_valid_buf;

always @(posedge clk) begin
	a_re_buf <= a_re;
	a_im_buf <= a_im;
	b_re_buf <= b_re;
	b_im_buf <= b_im;
    mul_valid_buf<=valid_in;
end

// DSP slice-ok
always @(posedge clk)
	if(rst) begin
		re_re<='b0;
		im_im<='b0;
		re_im<='b0;
		im_re<='b0;
		mul_valid<='b0;
	end else begin
		re_re<=a_re_buf*b_re_buf;
		im_im<=a_im_buf*b_im_buf;
		re_im<=a_re_buf*b_im_buf;
		im_re<=a_im_buf*b_re_buf;
		mul_valid<=mul_valid_buf;
	end

// kimeneti logika
always @(posedge clk)
	if(rst) begin
		p_re<='b0;
		p_im<='b0;
		valid_out<='b0;
	end else begin
		p_re<=re_re-im_im;
		p_im<=re_im+im_re;
		valid_out<=mul_valid;
	end

endmodule
