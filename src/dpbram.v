////////////////////////
// Dual-port blokk RAM
//
// A port: írás/olvasás
//   READ_FIRST üzemmód
// B port: csak olvasás
////////////////////////
module dpbram#(
	parameter ADDR_WIDTH=5,
	parameter DATA_WIDTH=32,
	parameter VALID_LEN=1
)(
	input clk,
	
	input [ADDR_WIDTH-1:0] addr_a,
	input [ADDR_WIDTH-1:0] addr_b,
	
	output reg [DATA_WIDTH-1:0] dout_a,
	output reg [DATA_WIDTH-1:0] dout_b,
	
	input [DATA_WIDTH-1:0] din_a,
	input wr_en_a,
	
	input [DATA_WIDTH-1:0] din_b,
	input wr_en_b,
	
	// Valid jel késleltető
	input [VALID_LEN-1:0] valid_in,
	output reg [VALID_LEN-1:0] valid_out
);

reg [DATA_WIDTH-1:0] data [2**ADDR_WIDTH-1:0];

always @(posedge clk) begin
	dout_a<=data[addr_a];
	if(wr_en_a)
		data[addr_a]<=din_a;
end

always @(posedge clk) begin
	dout_b<=data[addr_b];
	if(wr_en_b)
		data[addr_b]<=din_b;
end

always @(posedge clk) begin
	valid_out<=valid_in;
end

endmodule
