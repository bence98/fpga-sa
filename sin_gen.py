#!/usr/bin/env python3
from math import sin, pi
from sys import argv, stderr

if(len(argv)!=3):
	stderr.write("Használat: {} <FFT pontszám> <fájl>\n".format(argv[0]))
	exit(1)

pts=int(argv[1])
with open(argv[2], "w") as f:
	f.write("// {} pontos szinusz első negyedperiódusa ({} pont)\n// s.15 számábrázolási formátum\n// Generálta: {}\n".format(pts, pts//4, argv[0]))
	for i in range(pts//4):
		f.write(hex(int(sin(2*pi*i/pts)*2**15))[2:])
		f.write('\n')
