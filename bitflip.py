BIT_WIDTH=5

def bitflip(k):
	res=0
	for i in range(0,BIT_WIDTH):
		res+=(k%(2**(i+1))//(2**i))*(2**(BIT_WIDTH-1-i))
	return res
