`timescale 1 ns / 1 ps
module tf_twiddle_rom();

localparam LSB_VALUE=2.0**-15;

reg clk=0;

always #10
	clk=~clk;

reg [7:0] addr_tw=0;
wire signed [15:0] twiddle_re;
wire signed [15:0] twiddle_im;

always @(posedge clk)
	addr_tw<=addr_tw+1;

twiddle_rom uut(
	.clk(clk),
	.addr_tw(addr_tw),
	.twiddle_re(twiddle_re),
	.twiddle_im(twiddle_im)
);

real cos, sin;
always @(*) begin
	cos=twiddle_re*LSB_VALUE;
	sin=twiddle_im*LSB_VALUE;
end

endmodule
