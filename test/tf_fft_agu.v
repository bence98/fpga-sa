`timescale 1 ns / 1 ps
module tf_fft_agu();

reg clk=0;

always #10
	clk=~clk;

reg rst=1;
reg start=0;
reg [9:0] addr=0;
reg wr_en=0;
wire fft_done;
wire [31:0] rd_data;

initial #20
	rst=0;

initial #50
	wr_en=1;

initial #240
	start=1;

always @(posedge clk)
	if(fft_done)
		addr<=addr+1;

always @(posedge clk)
	if(fft_done)
		start=0;

fft uut(
	.clk(clk),
	.rst(rst),
	.start(start),
	.done(fft_done),
	.ex_rd_addr(addr),
	.ex_rd_data(rd_data),
	.ex_wr_addr('hb),
	.ex_wr_data('hcafe),
	.ex_wren(wr_en)
);

endmodule
