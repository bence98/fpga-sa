module tf_bfu();

reg clk=0;

always #10
	clk=~clk;

reg rst=1;
initial #20
	rst=0;

integer i=0;
always @(posedge clk)
	if(!rst) i<=i+1;

wire [7:0] addr_tw=i[15:8];
wire [15:0] twiddle_re;
wire [15:0] twiddle_im;

twiddle_rom rom(
	.clk(clk),
	.addr_tw(addr_tw),
	.twiddle_re(twiddle_re),
	.twiddle_im(twiddle_im)
);

wire [15:0] testpatterns [3:0];
assign testpatterns[0]='hfffe;
assign testpatterns[1]='hfffd;
assign testpatterns[2]='hfffb;
assign testpatterns[3]='h0;

wire [1:0] idx_a_re=i[1:0];
wire [1:0] idx_a_im=i[3:2];
wire [1:0] idx_b_re=i[5:4];
wire [1:0] idx_b_im=i[7:6];

butterfly_unit uut(
	.clk(clk),
	.rst(rst),
	.ai_re(testpatterns[idx_a_re]),
	.ai_im(testpatterns[idx_a_im]),
	.bi_re(testpatterns[idx_b_re]),
	.bi_im(testpatterns[idx_b_im]),
	.twiddle_re(twiddle_re),
	.twiddle_im(twiddle_im)
);

endmodule
