`timescale 1ns / 1ps

// Forrás: tb_codec_if
module tf_top();

reg clk = 1;
reg rst = 1;
wire codec_mclk;
wire codec_sclk;
wire codec_lrclk;
wire codec_sdout;

wire [31:0] sim_data;
reg  [ 4:0] sim_cntr;


top uut
(
   .clk100M       (clk),
   .rst           (rst),
   
   .fft_start     ('b1),
   .fft_wr_en     ('b1),
   
   .codec_m0      (),
   .codec_m1      (),
   .codec_i2s     (),
   .codec_mdiv1   (),
   .codec_mdiv2   (),
   .codec_rstb    (),
   .i2s_mclk      (codec_mclk),
   .i2s_lrclk     (codec_lrclk),
   .i2s_bclk      (codec_sclk),
   .i2s_dout      (),
   .i2s_data      (codec_sdout)
);


always #5
   clk <= ~clk;

initial
begin
   rst <= 1;
   #1020 
   rst <= 0;
end

always @ (negedge codec_lrclk)
   sim_cntr <= 5'b11111;
   
always @ (posedge codec_lrclk)   
   sim_cntr <= 5'b11111;

always @ (negedge codec_sclk)
   sim_cntr <= sim_cntr - 1;

assign sim_data = 32'h65432100;
assign codec_sdout = sim_data[sim_cntr];

endmodule
